package ru.pisarev.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    @NotNull
    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

    @NotNull
    public IndexIncorrectException(@NotNull final Throwable cause) {
        super(cause);
    }

    @NotNull
    public IndexIncorrectException(@NotNull final String value) {
        super("Error. This value `" + value + "` is not number.");
    }

}
