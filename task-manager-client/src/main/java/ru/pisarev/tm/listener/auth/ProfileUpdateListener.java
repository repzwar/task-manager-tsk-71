package ru.pisarev.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.SessionEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AuthAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class ProfileUpdateListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    @EventListener(condition = "@profileUpdateListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter first name");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        @Nullable final String middleName = TerminalUtil.nextLine();
        sessionEndpoint.updateUser(getSession(), firstName, lastName, middleName);
    }

}
