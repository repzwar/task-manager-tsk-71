package ru.pisarev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.endpoint.*;
import ru.pisarev.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Nullable
    private static SessionRecord session;

    @Nullable
    private ProjectRecord project;

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.open("user", "user");
    }

    @Before
    public void before() {
        project = new ProjectRecord();
        project.setName("Test");
        project.setId("1");
        projectEndpoint.addProject(session, project);
    }

    @After
    public void after() {
        projectEndpoint.removeProjectById(session, project.getId());
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.close(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Test", project.getName());

        @NotNull final ProjectRecord projectById = projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        @NotNull final List<ProjectRecord> projects = projectEndpoint.findProjectAll(session);
        Assert.assertTrue(projects.size() > 0);
    }


    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findAllByUserIdIncorrect() {
        projectEndpoint.findProjectAll(new SessionRecord());
    }

    @Test
    @Category(SoapCategory.class)
    public void findById() {
        @NotNull final ProjectRecord project = projectEndpoint.findProjectById(session, this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIdIncorrect() {
        @NotNull final ProjectRecord project = projectEndpoint.findProjectById(session, "34");
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdNull() {
        projectEndpoint.findProjectById(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdIncorrectUser() {
        projectEndpoint.findProjectById(new SessionRecord(), this.project.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByName() {
        @NotNull final ProjectRecord project = projectEndpoint.findProjectByName(session, "Test");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByNameIncorrect() {
        @NotNull final ProjectRecord project = projectEndpoint.findProjectByName(session, "34");
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameNull() {
        projectEndpoint.findProjectByName(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameIncorrectUser() {
        projectEndpoint.findProjectByName(new SessionRecord(), this.project.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIndex() {
        @NotNull final ProjectRecord project = projectEndpoint.findProjectByIndex(session, 0);
        Assert.assertNotNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrect() {
        projectEndpoint.findProjectByIndex(session, 34);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexNull() {
        projectEndpoint.findProjectByIndex(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrectUser() {
        projectEndpoint.findProjectByIndex(new SessionRecord(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        projectEndpoint.removeProjectById(session, project.getId());
        Assert.assertNull(projectEndpoint.findProjectById(session, project.getId()));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdNull() {
        projectEndpoint.removeProjectById(session, null);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdIncorrect() {
        projectEndpoint.removeProjectById(session, "34");
        Assert.assertNull(project);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdIncorrectUser() {
        projectEndpoint.removeProjectById(new SessionRecord(), this.project.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByName() {
        projectEndpoint.removeProjectByName(session, "Test");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrect() {
        projectEndpoint.removeProjectByName(session, "34");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameNull() {
        projectEndpoint.removeProjectByName(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrectUser() {
        projectEndpoint.removeProjectByName(new SessionRecord(), this.project.getName());
    }

}
