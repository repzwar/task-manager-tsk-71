package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.pisarev.tm.configuration.DatabaseConfiguration;
import ru.pisarev.tm.exception.empty.EmptyEmailException;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyLoginException;
import ru.pisarev.tm.marker.UnitCategory;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class UserServiceTest {

    @Nullable
    @Autowired
    private UserService userService;

    @Nullable
    private User user;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private static String USER_ID;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        @NotNull final User user = new User();
        user.setLogin("UserTest");
        user.setEmail("email");
        user.setPasswordHash("1");
        this.user = userService.add(user);
    }

    @Test
    @Category(UnitCategory.class)
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("UserTest", user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("email", user.getEmail());

        @NotNull final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertTrue(users.size() > 1);
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        @NotNull final User user = userService.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdIncorrect() {
        @NotNull final User user = userService.findById("34");
        Assert.assertNull(user);
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final User user = userService.findById(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLoginIncorrect() {
        @NotNull final User user = userService.findByLogin("34");
        Assert.assertNull(user);
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyLoginException.class)
    public void findByLoginNull() {
        @NotNull final User user = userService.findByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByEmailIncorrect() {
        @NotNull final User user = userService.findByEmail("34");
        Assert.assertNull(user);
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyEmailException.class)
    public void findByEmailNull() {
        @NotNull final User user = userService.findByEmail(null);
        Assert.assertNull(user);
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        userService.removeById(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        userService.removeById(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void isLoginExistFalse() {
        Assert.assertFalse(userService.isLoginExist("test"));
    }

    @Test
    @Category(UnitCategory.class)
    public void isEmailExistFalse() {
        Assert.assertFalse(userService.isEmailExist("e"));
    }

}